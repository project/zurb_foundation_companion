# ZURB Foundation companion

ZURB Foundation companion is a helper module for
the [ZURB Foundation theme](https://www.drupal.org/project/zurb_foundation).

## Features

* Provides a command to generate a subtheme with Drush:
`drush generate theme-zurb-foundation`

## Installation

Due to relying on external PHP libraries from packagist.org
to implement the subtheme generator
the module can only be installed using composer.

```
composer require drupal/zurb_foundation_companion:^6
```

This will also download the ZURB Foundation theme.

Have look at
[Composer template for Drupal projects](https://github.com/drupal-composer/drupal-project)
if you are not familiar on how to manage Drupal projects with composer.

## Subtheming

Drush 9 does not support commands coming from themes
so that is why this command lives in this module.
