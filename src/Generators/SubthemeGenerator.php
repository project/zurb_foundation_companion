<?php

namespace Drupal\zurb_foundation_companion\Generators;

use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * {@inheritdoc}
 */
class SubthemeGenerator extends BaseGenerator {
  /**
   * Drush command name.
   *
   * @var string
   */
  protected $name = 'd8:theme-zurb-foundation';
  /**
   * Drush command description.
   *
   * @var string
   */
  protected $description = 'Generates a ZURB Foundation subtheme.';
  /**
   * Drush alias.
   *
   * @var string
   */
  protected $alias = 'zurb-foundation';
  /**
   * The path of template.
   *
   * @var string
   */
  protected $templatePath = __DIR__;
  /**
   * Themes directory.
   *
   * @var string
   */
  protected $destination = 'themes';

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $questions['name'] = new Question('Theme name');
    $questions['name']->setValidator([Utils::class, 'validateRequired']);
    $questions['machine_name'] = new Question('Theme machine name');
    $questions['machine_name']->setValidator([Utils::class, 'validateMachineName']);
    $questions['description'] = new Question(
          'Description',
          'Custom sub-theme, inherits from the Foundation base theme read <a href=\"http://foundation.zurb.com/docs/index.php\">framework documentation</a>'
      );

    $vars = &$this->collectVars($input, $output, $questions);

    $prefix = 'custom/' . $vars['machine_name'] . '/';

    $zfPath = DRUPAL_ROOT . '/' . drupal_get_path('theme', 'zurb_foundation') . '/';

    $patterns = array('/^name: .*$/m', '/^description: .*$/m', '/STARTER/');
    $replacements = array('name: ' . $vars['name'], 'description: "' . addslashes($vars['description']) . '<br><a href=\"http://foundation.zurb.com/docs/index.php\">framework documentation</a>"', $vars['machine_name']);
    $this->addFile()
      ->path($prefix . '{machine_name}.info.yml')
      ->content(preg_replace($patterns, $replacements, file_get_contents(
              $zfPath . 'STARTER/STARTER.info.yml.txt'
            )));

    $this->addFile()
      ->path($prefix . '{machine_name}.libraries.yml')
      ->content(str_replace('STARTER', $vars['machine_name'], file_get_contents(
              $zfPath . 'STARTER/STARTER.libraries.yml'
            )));

    $this->addFile()
      ->path($prefix . '{machine_name}.theme')
      ->content(str_replace('STARTER', $vars['machine_name'], file_get_contents(
              $zfPath . 'STARTER/STARTER.theme'
            )));

    $this->addFile()
      ->path($prefix . 'config/install/README.txt')
      ->content(file_get_contents(
              $zfPath . 'STARTER/config/install/README.txt'
            ));

    $this->addFile()
      ->path($prefix . 'css/{machine_name}.css')
      ->content(str_replace('STARTER', $vars['machine_name'], file_get_contents(
              $zfPath . 'STARTER/css/STARTER.css'
            )));

    $gif_files = glob($zfPath . 'STARTER/images/foundation/orbit/*.gif');
    foreach ($gif_files as $gif_file) {
      $this->addFile()
        ->path($prefix . 'images/foundation/orbit/' . basename($gif_file))
        ->content(file_get_contents($gif_file));
    }

    $jpg_files = glob($zfPath . 'STARTER/images/foundation/orbit/*.jpg');
    foreach ($jpg_files as $jpg_file) {
      $this->addFile()
        ->path($prefix . 'images/foundation/orbit/' . basename($jpg_file))
        ->content(file_get_contents($jpg_file));
    }

    $png_files = glob($zfPath . 'STARTER/images/foundation/orbit/*.png');
    foreach ($png_files as $png_file) {
      $this->addFile()
        ->path($prefix . 'images/foundation/orbit/' . basename($png_file))
        ->content(file_get_contents($png_file));
    }

    $this->addFile()
      ->path($prefix . 'js/{machine_name}.js')
      ->content(file_get_contents(
              $zfPath . 'STARTER/js/STARTER.js'
            ));

    $this->addFile()
      ->path($prefix . 'scss/{machine_name}.scss')
      ->content(file_get_contents(
              $zfPath . 'STARTER/scss/STARTER.scss'
            ));

    $this->addFile()
      ->path($prefix . 'scss/_settings.scss')
      ->content(file_get_contents(
              $zfPath . 'STARTER/scss/_settings.scss'
            ));

    $scss_base_files = glob($zfPath . 'STARTER/scss/base/*.scss');
    foreach ($scss_base_files as $scss_base_file) {
      $this->addFile()
        ->path($prefix . 'scss/base/' . basename($scss_base_file))
        ->content(file_get_contents($scss_base_file));
    }

    $scss_layout_files = glob($zfPath . 'STARTER/scss/layout/*.scss');
    foreach ($scss_layout_files as $scss_layout_file) {
      $this->addFile()
        ->path($prefix . 'scss/layout/' . basename($scss_layout_file))
        ->content(file_get_contents($scss_layout_file));
    }

    $scss_modules_files = glob($zfPath . 'STARTER/scss/modules/*.scss');
    foreach ($scss_modules_files as $scss_modules_file) {
      $this->addFile()
        ->path($prefix . 'scss/modules/' . basename($scss_modules_file))
        ->content(file_get_contents($scss_modules_file));
    }

    $scss_states_files = glob($zfPath . 'STARTER/scss/states/*.scss');
    foreach ($scss_states_files as $scss_states_file) {
      $this->addFile()
        ->path($prefix . 'scss/states/' . basename($scss_states_file))
        ->content(file_get_contents($scss_states_file));
    }

    $scss_theme_files = glob($zfPath . 'STARTER/scss/theme/*.scss');
    foreach ($scss_theme_files as $scss_theme_file) {
      $this->addFile()
        ->path($prefix . 'scss/theme/' . basename($scss_theme_file))
        ->content(file_get_contents($scss_theme_file));
    }

    $this->addFile()
      ->path($prefix . 'templates/README.txt')
      ->content(file_get_contents(
              $zfPath . 'STARTER/templates/README.txt'
            ));

    $this->addFile()
      ->path($prefix . '.gitattributes')
      ->content(file_get_contents(
              $zfPath . 'STARTER/.gitattributes'
            ));

    $this->addFile()
      ->path($prefix . '.gitignore')
      ->content(file_get_contents(
              $zfPath . 'STARTER/.gitignore'
            ));

    $this->addFile()
      ->path($prefix . '.scss-lint.yml')
      ->content(file_get_contents(
              $zfPath . 'STARTER/.scss-lint.yml'
            ));

    $this->addFile()
      ->path($prefix . 'README.txt')
      ->content(file_get_contents(
              $zfPath . 'STARTER/README.txt'
            ));

    $this->addFile()
      ->path($prefix . 'gulpfile.js')
      ->content(file_get_contents(
              $zfPath . 'STARTER/gulpfile.js'
            ));

    $this->addFile()
      ->path($prefix . 'package.json')
      ->content(str_replace('STARTER', $vars['machine_name'], file_get_contents(
              $zfPath . 'STARTER/package.json'
          )));

    $this->addFile()
      ->path($prefix . 'config.js')
      ->content(file_get_contents(
              $zfPath . 'STARTER/config.js'
          ));

    $this->addFile()
      ->path($prefix . 'theme-settings.php')
      ->content(file_get_contents(
              $zfPath . 'STARTER/theme-settings.php'
          ));

    $this->addFile()
      ->path($prefix . 'logo.svg')
      ->content(file_get_contents(
              $zfPath . 'STARTER/logo.svg'
          ));
  }

}
